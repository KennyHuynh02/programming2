import java.util.Scanner;
class PartThree {
	public static void main(String[]args){
		Scanner keyboard = new Scanner(System.in);
		System.out.println("enter your first value");
		double userInput1 = keyboard.nextDouble();
		System.out.println("enter your second value");
		double userInput2 = keyboard.nextDouble();
		
		double add = Calculator.add(userInput1, userInput2);
		double subtract = Calculator.subtract(userInput1, userInput2);
		Calculator operations = new Calculator();
		double multiply = operations.multiply(userInput1, userInput2);
		double divide = operations.divide(userInput1, userInput2);
		
		System.out.println(add);
		System.out.println(subtract);
		System.out.println(multiply);
		System.out.println(divide);
	}
}