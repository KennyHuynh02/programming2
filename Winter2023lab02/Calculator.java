class Calculator {
	public static double add (double number1, double number2){
		double operation = number1+number2;
		return operation;
	}
	
	public static double subtract (double number1, double number2){
		double operation = number1-number2;
		return operation;
	}
	
	public double multiply (double number1, double number2){
		double operation = number1*number2;
		return operation;
	}
	
	public double divide (double number1, double number2){
		double operation = number1/number2;
		return operation;
	}
	
}