class MethodsTests {
	public static void main(String[]args){
		int x = 5;
		System.out.println(x);
		methodNoInputNoReturn();
		System.out.println(x);
		System.out.println(x);
		methodOneInputNoReturn(x+10);
		System.out.println(x);
		methodTwoInputNoReturn(1, 1.0);
		int z = methodNoInputReturnInt();
		System.out.println(z);
		double squareRoot = sumSquareRoot(9, 5);
		System.out.println(squareRoot);
		String s1 = "java";
		String s2 = "programming";
		System.out.println(s1.length());
		System.out.println(s2.length());
		int method1 = SecondClass.addOne(50);
		System.out.println(method1);
		
		SecondClass sc = new SecondClass();
		System.out.println(sc.addTwo(50));
	}
	public static void methodNoInputNoReturn(){
		System.out.println("I'm in a method that takes no input and returns nothing");
		int x = 20;
		System.out.println(x);
	}
	
	public static void methodOneInputNoReturn(int number){
		number -=5;
		System.out.println("Inside the method one input no return");
		
	}
	
	public static void methodTwoInputNoReturn (int numInteger, double numDouble){
		System.out.println(numInteger);
		System.out.println(numDouble);
	}
	
	public static int methodNoInputReturnInt(){
		int number = 5;
		return number;
	}
	
	public static double sumSquareRoot(int number1, int number2){
		int sum = number1+number2;
		double squareRoot = Math.sqrt(sum);
		return squareRoot;
	}
}